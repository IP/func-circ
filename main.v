`timescale 1ns / 1ps

module main;
    reg clock;
    reg reset;
    reg [7:0] a;
    reg [7:0] b;
    reg start;
    wire busy;
    wire [7:0] y;
    wire [7:0] expected_y;

    func func_1 (clock, reset, a, b, start, busy, y);
    mock mock_1 (a, b, expected_y);

    integer ai;
    integer bi;
    integer fails;

    initial begin
        fails = 0;
        
        $display("| start");
        $display("| a | b | y | exp | ok |");

        reset = 1;
        
        #5 clock = 1;
        #5 clock = 0;
        
        for (ai = 0; ai < 256; ai = ai + 1) begin
            for (bi = 0; bi < 256; bi = bi + 1) begin
                a = ai;
                b = bi;
                
                reset = 0;
                start = 1;
                #5 clock = 1;
                #5 clock = 0;
                
                start = 0;
                
                while(busy) begin
                    #5 clock = !clock;
                end
                
                $display(
                    "| ", a,
                    "| ", b,
                    "| ", y,
                    "| ", expected_y,
                    "| ", (y == expected_y),
                    "|"
                );
                
                if (y != expected_y) begin
                    fails = fails + 1;
                end
            end
        end
        
        $display("| finish");
        $display("| fails: ", fails);
        $finish;
    end
endmodule
