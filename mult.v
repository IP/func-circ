`timescale 1ns / 1ps

module mult (
    input clock,
    input reset,
    input [7:0] a_input,
    input [7:0] b_input,
    input start,
    output busy,
    output reg [15:0] y
);
    localparam IDLE = 1'b0;
    localparam WORK = 1'b1;
    
    reg [2:0] ctr;
    wire [2:0] end_step;
    wire [7:0] part_sum;
    wire [15:0] shifted_part_sum;
    reg [7:0] a, b;
    reg [15:0] part_res;
    reg state;
    assign part_sum = a & {8{b[ctr]}};
    assign shifted_part_sum = part_sum << ctr;
    assign end_step = (ctr == 3'h7);
    assign busy = state;
    
    always @(posedge clock)
        if (reset) begin
            ctr <= 0;
            part_res <= 0;
            y <= 0;
            state <= IDLE;
        end
        else begin
            case (state)
                IDLE:
                    if (start) begin
                        state <= WORK;
                        a <= a_input;
                        b <= b_input;
                        ctr <= 0;
                        part_res <= 0;
                    end
                WORK: begin
                    if (end_step) begin
                        state <= IDLE;
                        y <= part_res;
                    end
                    part_res <= part_res + shifted_part_sum;
                    ctr <= ctr + 1;
                end
            endcase
        end
endmodule
