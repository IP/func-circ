
module func(
    input clock,
    input reset,
    input [7:0] a_input,
    input [7:0] b_input,
    input start,
    output busy,
    output reg [7:0] y
);
    localparam IDLE = 4'h0;
    localparam CBRT0 = 4'h1;
    localparam CBRT1 = 4'h2;
    localparam CBRT2 = 4'h3;
    localparam CBRT3 = 4'h4;
    localparam CBRT4 = 4'h5;
    localparam CBRT5 = 4'h6;
    localparam CBRT6 = 4'h7;
    localparam ADD0 = 4'h8;
    localparam ADD1 = 4'h9;
    localparam SQRT0 = 4'hA;
    localparam SQRT1 = 4'hB;
    localparam SQRT2 = 4'hC;
    localparam SQRT3 = 4'hD;
    reg [7:0] x;
    reg [7:0] a;
    reg [7:0] m;
    reg [7:0] b;
    reg [3:0] state;
    wire signed [7:0] s;
    wire [7:0] double_y;
    wire [7:0] after_mul;

    reg mul_reset;
    reg mul_start;
    wire mul_busy;
    wire [15:0] raw_mul_out;
    wire [7:0] mul_out;

    reg [7:0] add_a;
    reg [7:0] add_b;
    reg add_c;
    wire [7:0] add_out;

    assign s = m;
    assign double_y = y << 1;
    assign after_mul = ((mul_out + (mul_out << 1)) | 1) << s; // 1st adder
    assign busy = (state != IDLE);
assign mul_out = raw_mul_out[7:0];

    mult multiplier(clock, reset, b, y, mul_start, mul_busy, raw_mul_out);
    assign add_out = add_a + add_b + add_c; // 2nd full adder

    always @(posedge clock) begin
        if (reset) begin
            x <= 0;
            m <= 0;
            b <= 0;
            y <= 0;
            add_a <= 0;
            add_b <= 0;
            add_c <= 0;
            state <= IDLE;
        end
        else begin
            case (state)
            IDLE:
                if (start) begin
                    state <= CBRT0;
                    y <= 0;
                    x <= b_input;
                    a <= a_input;
                    m <= 8'd6;
                end
            CBRT0: begin
                if (s > -3) begin
                    state <= CBRT1;
                end
                else begin
                    state <= ADD0;
                end
            end
            CBRT1: begin
                y <= double_y;
                b <= double_y | 1;
                mul_start <= 1;
                state <= CBRT2;
            end
            CBRT2: begin
                if (mul_busy) begin
                    mul_start <= 0;
                    state <= CBRT3;
                end
            end
            CBRT3: begin
                if (!mul_busy) begin
                    b <= after_mul;
                    state <= CBRT4;
                end
            end
            CBRT4: begin
                add_a <= x;
                add_b <= ~b;
                add_c <= 1;
                state <= CBRT5;
            end
            CBRT5: begin
                if (x >= b) begin
                    x <= add_out;
                    y <= y | 1;
                end
                add_a <= s;
                add_b <= -3;
                add_c <= 0;
                state <= CBRT6;
            end
            CBRT6: begin
                m <= add_out; // actually s <= add_out
                state <= CBRT0;
            end
            ADD0: begin
                add_a <= y;
                add_b <= a;
                add_c <= 0;
                state <= ADD1;
            end
            ADD1: begin
                x <= add_out;
                y <= 0;
                m <= 8'd64;
                state <= SQRT0;
            end
          SQRT0: begin
                if (m != 0) begin
                    state <= SQRT1;
                end
                else begin
                    state <= IDLE;
                end
            end
            SQRT1: begin
                b <= y | m;
                y <= (y >> 1);
                state <= SQRT2;
            end
            SQRT2: begin
                add_a <= x;
                add_b <= ~b;
                add_c <= 1;
                state <= SQRT3;
            end
            SQRT3: begin
                if (x >= b) begin
                    x <= add_out;
                    y <= y | m;
                end
                m <= (m >> 2);
                state <= SQRT0;
            end
            endcase
        end
    end
endmodule
