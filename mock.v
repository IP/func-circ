`timescale 1ns / 1ps

module mock(
    input [7:0] a,
    input [7:0] b,
    output [7:0] y
);
    wire [7:0] cbrt;
    wire [7:0] sum;
    assign cbrt =
        (b >= 216) ? 6 :
        (b >= 125) ? 5 :
        (b >= 64) ? 4 :  
        (b >= 27) ? 3 :
        (b >= 8) ? 2 :
        (b >= 1) ? 1 :
        0;
    
    assign sum = a + cbrt;
    
    assign y =
        (sum >= 225) ? 15 :
        (sum >= 196) ? 14 :
        (sum >= 169) ? 13 :
        (sum >= 144) ? 12 :
        (sum >= 121) ? 11 :
        (sum >= 100) ? 10 :
        (sum >= 81) ? 9 :
        (sum >= 64) ? 8 :
        (sum >= 49) ? 7 :
        (sum >= 36) ? 6 :
        (sum >= 25) ? 5 :
        (sum >= 16) ? 4 :
        (sum >= 9) ? 3 :
        (sum >= 4) ? 2 :
        (sum >= 1) ? 1 :
        0;
endmodule
